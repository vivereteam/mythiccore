Permissions for this plugin:

roll.info.op - This allows you to run all staff related commands. Soon to be depricated.

mythic.core.(channel).read - Allows you to read a channel
mythic.core.(channel).send - Allows you to send messages to a channel
mythic.core.(channel).focus - Allows you to focus a channel