package net.zachariah.mythic.core.character;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.OfflinePlayer;

public class MythicCharacters {

    private final MythicMain plugin;

    public MythicCharacters(MythicMain plugin) {
        this.plugin = plugin;
    }

    public MythicCharacter get(OfflinePlayer player) {
        return new MythicCharacter(plugin, player);
    }

}
