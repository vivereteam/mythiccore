package net.zachariah.mythic.core.character;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;

public class MythicCharacter {

    private final MythicMain plugin;
    private final OfflinePlayer player;
    private final File characterFile;
    private YamlConfiguration characterConfig;

    public MythicCharacter(MythicMain plugin, OfflinePlayer player) {
        this.plugin = plugin;
        this.player = player;
        File characterDirectory = new File(plugin.getDataFolder(), "players");
        if (!characterDirectory.exists()) {
            characterDirectory.mkdirs();
        }
        this.characterFile = new File(characterDirectory, player.getUniqueId().toString() + ".yml");
        this.characterConfig = YamlConfiguration.loadConfiguration(characterFile);
        if (!characterFile.exists()) {
//            migrate();
        }
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public String getNick() { return characterConfig.getString("user.nick"); }

    public String getChannel() {
        return characterConfig.getString("user.channel");
    }

    public void setChannel(String id) {
        characterConfig.set("user.channel", id);
        save();
    }

    public void setMute(String id, boolean mute){
        characterConfig.set("user." + id + ".muted", mute);
        save();
    }

    public boolean hasChannelMuted(String id){
        return characterConfig.getBoolean("user." + id + ".muted");
    }

    public void setNick(String nick){
        characterConfig.set("user.nick", nick);
        save();
    }

    public Boolean isOrc() { return characterConfig.getBoolean("user.isOrc"); }

    public void setIsOrc(boolean bool){
        characterConfig.set("user.isOrc", bool);
        save();
    }

    public String getName() {
        return characterConfig.getString("card.name");
    }

    public void setName(String name) {
        characterConfig.set("card.name", name);
        save();
    }

    public void setClassName(String clazz){
        characterConfig.set("stats.class", clazz);
        save();
    }

    public String getClassName() {
        return characterConfig.getString("stats.class");
    }

    public String getAlignment() {
        return characterConfig.getString("card.alignment");
    }

    public void setAlignment(String alignment) {
        characterConfig.set("card.alignment", alignment);
        save();
    }

    public String getDescription() {
        return characterConfig.getString("card.description");
    }

    public void setDescription(String description) {
        characterConfig.set("card.description", description);
        save();
    }

    public String getRace() {
        return characterConfig.getString("card.race");
    }

    public void setRace(String race) {
        characterConfig.set("card.race", race);
        characterConfig.set("card.race", race);
        save();
    }

    public String getLang() { return characterConfig.getString("card.languages"); }

    public void setLang(String lang){
        characterConfig.set("card.languages", lang);
        save();
    }

    public String getGender() {
        return characterConfig.getString("card.gender");
    }

    public void setGender(String gender) {
        characterConfig.set("card.gender", gender);
        save();
    }

    public String getProfession() {
        return characterConfig.getString("card.profession");
    }

    public void setProfession(String profession) {
        characterConfig.set("card.profession", profession);
        save();
    }

    public String getArmorProf() {
        return characterConfig.getString("proficiencies.armor");
    }

    public void setArmorProf(String armor){
        characterConfig.set("proficiencies.armor", armor);
        save();
    }

    public String getWeaponProf() {
        return characterConfig.getString("proficiencies.weapon");
    }

    public void setWeaponProf(String weapon){
        characterConfig.set("proficiencies.weapon", weapon);
        save();
    }

    public String getToolProf() {
        return characterConfig.getString("proficiencies.tool");
    }

    public void setToolProf(String tool){
        characterConfig.set("proficiencies.tool", tool);
        save();
    }

    public String getSkillProf() {
        return characterConfig.getString("proficiencies.skill");
    }

    public void setSkillProf(String skill){
        characterConfig.set("proficiencies.skill", skill);
        save();
    }

    public int getStatValue(MythicStat stat) {
        return characterConfig.getInt("stats." + stat.getConfigSectionName());
    }

    public void setStatValue(MythicStat stat, int value) {
        characterConfig.set("stats." + stat.getConfigSectionName(), value);
        save();
    }

    private void save() {
        try {
            characterConfig.save(characterFile);
        } catch (IOException exception) {
            plugin.getLogger().log(Level.SEVERE, "Failed to save character", exception);
        }
    }

//    private void migrate() {
//        setName(plugin.getCharConfig().getString("cards." + player.getName() + ".name"));
//        setAlignment(plugin.getCharConfig().getString("cards." + player.getName() + ".alignment"));
//        setDescription(plugin.getCharConfig().getString("cards." + player.getName() + "description"));
//        setRace(plugin.getCharConfig().getString("cards." + player.getName() + ".race"));
//        setGender(plugin.getCharConfig().getString("cards." + player.getName() + ".gender"));
//        setProfession(plugin.getCharConfig().getString("cards." + player.getName() + ".profession"));
//        Arrays.stream(MythicStat.values()).forEach(stat -> {
//            setStatValue(stat, plugin.getStatConfig().getInt("stats." + player.getName() + "." + stat.getConfigSectionName()));
//        });
       // Arrays.stream(VivereFeatTier.values()).forEach(tier -> {
         //   Arrays.stream(VivereFeatSlot.values()).forEach(slot -> {
           //     setFeat(tier, slot, plugin.getFeatConfig().getString("feats." + player.getName() + "." + tier.getConfigSectionName() + "." + slot.getConfigSectionName()));
           // });
        //});
//    }

}
