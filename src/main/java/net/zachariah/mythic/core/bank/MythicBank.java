package net.zachariah.mythic.core.bank;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.inventory.MythicItemStack;
import net.zachariah.mythic.core.inventory.MythicMaterial;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Zac on 2017-05-31.
 */
public class MythicBank implements Listener {

    private final MythicMain plugin;

    public MythicBank(MythicMain instance) {
        plugin = instance;
    }


    public void upMoney(Player player, MythicMaterial material, int ammount) {
        ItemStack itemStack = new MythicItemStack(material, ammount).toItemStack();
        player.getInventory().addItem(itemStack);
    }

    public void upMoney(Player player, MythicMaterial material) {
        upMoney(player, material, 1);
    }

}