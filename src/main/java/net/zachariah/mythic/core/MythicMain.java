package net.zachariah.mythic.core;

import net.zachariah.mythic.core.bank.MythicBank;
import net.zachariah.mythic.core.calendar.MythicCalendar;
import net.zachariah.mythic.core.character.MythicCharacters;
import net.zachariah.mythic.core.chat.MythicChat;
import net.zachariah.mythic.core.command.charcardcommands.*;
import net.zachariah.mythic.core.command.chatcommands.ChannelCommand;
import net.zachariah.mythic.core.command.chatcommands.MuteCommand;
import net.zachariah.mythic.core.command.combatcommands.RollorderCommand;
import net.zachariah.mythic.core.command.expcommands.ExpCheckCommand;
import net.zachariah.mythic.core.command.expcommands.ExpGiveCommand;
import net.zachariah.mythic.core.command.expcommands.ExpSetCommand;
import net.zachariah.mythic.core.command.memecommands.*;
import net.zachariah.mythic.core.command.potioneffectcommands.LeapCommand;
import net.zachariah.mythic.core.command.potioneffectcommands.NightVisionCommand;
import net.zachariah.mythic.core.command.potioneffectcommands.WaterbreathingCommand;
import net.zachariah.mythic.core.command.proficienciescommands.*;
import net.zachariah.mythic.core.command.rollcommands.DMRollCommand;
import net.zachariah.mythic.core.command.rollcommands.RollCommand;
import net.zachariah.mythic.core.command.rollcommands.RollInfoCommand;
import net.zachariah.mythic.core.command.setstatcommands.*;
import net.zachariah.mythic.core.command.staffutilcommands.FreezeCommand;
import net.zachariah.mythic.core.command.staffutilcommands.UnfreezeCommand;
import net.zachariah.mythic.core.command.staffutilcommands.VibeCheckCommand;
import net.zachariah.mythic.core.command.statpoolcommands.*;
import net.zachariah.mythic.core.command.utilcommands.DuratagCommand;
import net.zachariah.mythic.core.command.utilcommands.NickCommand;
import net.zachariah.mythic.core.command.utilcommands.UnstuckCommand;
import net.zachariah.mythic.core.command.utilcommands.VivereHelpCommand;
import net.zachariah.mythic.core.listener.*;
import net.zachariah.mythic.core.rigger.MythicRigger;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MythicMain extends JavaPlugin {

    private MythicCharacters characters;
    private MythicCalendar calendar;
//    private DiceRoller roller;
    private MythicBank bank;
    private MythicRigger rigger;
    private File configf, statsf, charf;
    @SuppressWarnings("unused")
    //'config' left unused for future endeavors
    private FileConfiguration config, stat, charinfo, mining, ore;
    private List<String> channels = new ArrayList<>();
    private HashMap<String, String> channelAliases = new HashMap<>();


    private void createFiles() {
        configf = new File(getDataFolder(), "config.yml");

        if (!configf.exists()) {
            configf.getParentFile().mkdirs();
            saveResource("config.yml", false);
        }

        config = YamlConfiguration.loadConfiguration(configf);
    }

    private void registerChannels() {
        for (File file : new File(getDataFolder(), "channels").listFiles()){
            String id = file.getName().replace(".yml", "");
            channels.add(id);
            MythicChat channel = new MythicChat(this, id);
            channelAliases.put(channel.getAlias(), id);

        }
    }

    public void onEnable() {

        getServer().getWorlds().forEach(world -> world.setTime(9200L));

        createFiles();
        registerChannels();

        getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            for (World world : getServer().getWorlds()) {
                world.setTime(world.getTime() - 23L);
            }
        }, 0L, 24L);



        saveDefaultConfig();


        characters = new MythicCharacters(this);
        calendar = new MythicCalendar();
        //roller = new DiceRoller(this);
        bank = new MythicBank(this);

        rigger = new MythicRigger();

        getServer().getPluginManager().registerEvents(new SignChangeListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerInteractEntityListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityDamageListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerExpChangeEventListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerChatEventListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerCommandPreprocessListener(this), this);

        getCommand("vunstuck").setExecutor(new UnstuckCommand(this));
        getCommand("viverehelp").setExecutor(new VivereHelpCommand(this));
        getCommand("duratag").setExecutor(new DuratagCommand(this));
        getCommand("nick").setExecutor(new NickCommand(this));

        getCommand("checkexp").setExecutor(new ExpCheckCommand(this));
        getCommand("addexp").setExecutor(new ExpGiveCommand(this));
        getCommand("setexp").setExecutor(new ExpSetCommand(this));

        getCommand("setclass").setExecutor(new SetClassCommand(this));
        getCommand("setcharisma").setExecutor(new SetCharismaCommand(this));
        getCommand("setconstitution").setExecutor(new SetConstitutionCommand(this));
        getCommand("setwisdom").setExecutor(new SetWisdomCommand(this));
        getCommand("setstrength").setExecutor(new SetStrengthCommand(this));
        getCommand("setdexterity").setExecutor(new SetDexterityCommand(this));
        getCommand("setintelligence").setExecutor(new SetIntelligenceCommand(this));
        getCommand("sethp").setExecutor(new SetHPCommand(this));

        getCommand("proficiencies").setExecutor(new ProficienciesCommand(this));
        getCommand("setweaponp").setExecutor(new SetWeaponProfCommand(this));
        getCommand("settoolp").setExecutor(new SetToolProfCommand(this));
        getCommand("setarmorp").setExecutor(new SetArmorProfCommand(this));
        getCommand("setskills").setExecutor(new SetSkillsCommand(this));

        getCommand("currenthp").setExecutor(new CurrentHPCommand(this));
        getCommand("checkhp").setExecutor(new CheckHPCommand(this));
        getCommand("restorehp").setExecutor(new RestoreHPCommand(this));
        getCommand("healhp").setExecutor(new HealHPCommand(this));
        getCommand("damagehp").setExecutor(new DamageHPCommand(this));
        getCommand("temphp").setExecutor(new SetTempHPCommand(this));

        getCommand("rollorder").setExecutor(new RollorderCommand(this));

        getCommand("rollinfo").setExecutor(new RollInfoCommand(this));
        getCommand("char").setExecutor(new CharCommand(this));
        getCommand("setalignment").setExecutor(new SetAlignmentCommand(this));
        getCommand("setname").setExecutor(new SetNameCommand(this));
        getCommand("setrace").setExecutor(new SetRaceCommand(this));
        getCommand("setpro").setExecutor(new SetProCommand(this));
        getCommand("setgen").setExecutor(new SetGenCommand(this));
        getCommand("setdes").setExecutor(new SetDesCommand(this));
        getCommand("setlanguages").setExecutor(new SetLangCommand(this));
        getCommand("adddes").setExecutor(new AddDesCommand(this));
        getCommand("resetpro").setExecutor(new ResetProCommand(this));
        getCommand("resetalignment").setExecutor(new ResetAlignmentCommand(this));

        getCommand("tem").setExecutor(new TemCommand());
        getCommand("rhomb").setExecutor(new RhombCommand());
        getCommand("rig").setExecutor(new RigCommand(this));
        getCommand("ziggurat").setExecutor(new ZugguratCommand());
        getCommand("scotland").setExecutor(new ScotlandCommand());
        getCommand("omega").setExecutor(new BoomerCommand());
        getCommand("orc").setExecutor(new OrcCommand(this));

        getCommand("calendar").setExecutor(calendar);

        getCommand("freeze").setExecutor(new FreezeCommand(this));
        getCommand("unfreeze").setExecutor(new UnfreezeCommand());
        getCommand("vibecheck").setExecutor(new VibeCheckCommand(this));

        getCommand("nightvision").setExecutor(new NightVisionCommand(this));
        getCommand("aqualung").setExecutor(new WaterbreathingCommand(this));
        getCommand("leap").setExecutor(new LeapCommand(this));

        getCommand("dmroll").setExecutor(new DMRollCommand(this));
        getCommand("roll").setExecutor(new RollCommand(this));

        getCommand("ch").setExecutor(new ChannelCommand(this));
        getCommand("chmute").setExecutor(new MuteCommand(this));

        ScoreboardManager manager = Bukkit.getScoreboardManager();

        try{
            Team mNoCol = manager.getMainScoreboard().registerNewTeam("NoCol");
        } catch (IllegalArgumentException e){
            // do nothing
        }

        Team mNoCol = manager.getMainScoreboard().getTeam("NoCol");
        if (getConfig().get("VibeCheck").equals(true)){
            mNoCol.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.ALWAYS);
        }
        if (getConfig().get("VibeCheck").equals(false)){
            mNoCol.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        }



    }

    @Override
    public void onDisable() {

    }

    public MythicCharacters getCharacters() {
        return characters;
    }

    public MythicBank getBank() {
        return bank;
    }

    public MythicRigger getRigger() {
        return rigger;
    }

    public void announceMessage(List<UUID> uuids, String str) {
        for (UUID uuid : uuids) {
            Bukkit.getPlayer(uuid).sendMessage(str);
        }
    }

    public List<String> getChannels() {
        return channels;
    }

    public HashMap<String, String> getChannelAliases() {
        return channelAliases;
    }

}

