package net.zachariah.mythic.core.inventory;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public enum MythicMaterial {

    COPPER_COIN(
            Material.MAGMA_CREAM,
            ChatColor.WHITE + "Copper coin",
            "Well would you look",
            "at that, a coin on the",
            "road!"
    ),
    SILVER_COIN(
            Material.QUARTZ,
            ChatColor.WHITE + "Silver coin",
            "A regular, silver coin.",
            "It's worth keeping these",
            "around."
            ),
    GOLD_COIN(
            Material.GHAST_TEAR,
            ChatColor.WHITE + "Gold coin",
            "An extremely valuable",
            "coin. Possibly worth a",
            "man's loyalty, or blood."
            ),
    PLATINUM_COIN(
            Material.PRISMARINE_CRYSTALS,
            ChatColor.WHITE + "Platinum coin",
            "A coin worth more than",
            "most men would hold",
            "at any one time."
            );

    private Material bukkitMaterial;
    private String displayName;
    private String[] lore;

    MythicMaterial(Material bukkitMaterial, String displayName, String... lore) {
        this.bukkitMaterial = bukkitMaterial;
        this.displayName = displayName;
        this.lore = lore;
    }

    public Material getBukkitMaterial() {
        return bukkitMaterial;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String[] getLore() {
        return lore;
    }

    public boolean isMaterial(ItemStack itemStack) {
        return itemStack.hasItemMeta()
                && itemStack.getItemMeta().hasDisplayName()
                && itemStack.getItemMeta().hasLore()
                && itemStack.getType() == getBukkitMaterial()
                && itemStack.getItemMeta().getDisplayName().equals(getDisplayName())
                && Arrays.equals(itemStack.getItemMeta().getLore().toArray(), getLore());
    }

    public void applyTo(ItemStack itemStack) {
        itemStack.setType(getBukkitMaterial());
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(getDisplayName());
        meta.setLore(Arrays.asList(getLore()));
        itemStack.setItemMeta(meta);
    }

    public static MythicMaterial getMaterial(ItemStack itemStack) {
        for (MythicMaterial material : values()) {
            if (material.isMaterial(itemStack)) {
                return material;
            }
        }
        return null;
    }

    public static MythicMaterial getByName(String name) {
        for (MythicMaterial material : values()) {
            if (material.getDisplayName().equals(name)) {
                return material;
            }
        }
        return null;
    }

}
