package net.zachariah.mythic.core.inventory;

import org.bukkit.inventory.ItemStack;

public class MythicItemStack {

    private MythicMaterial type;
    private int amount;

    public MythicItemStack(MythicMaterial type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    public ItemStack toItemStack() {
        ItemStack itemStack = new ItemStack(type.getBukkitMaterial(), amount);
        type.applyTo(itemStack);
        return itemStack;
    }

    public static MythicItemStack fromItemStack(ItemStack itemStack) {
        MythicMaterial material = MythicMaterial.getMaterial(itemStack);
        if (material != null) {
            return new MythicItemStack(material, itemStack.getAmount());
        }
        return null;
    }

}
