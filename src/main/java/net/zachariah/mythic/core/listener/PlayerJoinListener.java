package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import net.zachariah.mythic.core.character.MythicCharacter;

/**
 * Created by Zac on 2018-02-02.
 */
public class PlayerJoinListener implements Listener {

    final MythicMain plugin;

    public PlayerJoinListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    ScoreboardManager manager = Bukkit.getScoreboardManager();

    @EventHandler
    public void PlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        MythicCharacter character = plugin.getCharacters().get(player);

        player.setDisplayName(character.getNick());
        player.setScoreboard(manager.getMainScoreboard());
        Scoreboard main = manager.getMainScoreboard();
        Team mNoCol = main.getTeam("NoCol");
        mNoCol.addEntry(player.getName());

        }


}
