package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

public class PlayerExpChangeEventListener implements Listener {

    private final MythicMain plugin;

    public PlayerExpChangeEventListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void PlayerExpChange(PlayerExpChangeEvent event){
        // This is so that Players will not recieve EXP from any other way
        event.setAmount(0);

    }

}
