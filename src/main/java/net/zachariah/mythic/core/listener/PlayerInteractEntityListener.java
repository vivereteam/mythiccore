package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * Created by Zac on 2017-06-29.
 */
public class PlayerInteractEntityListener implements Listener {

    private final MythicMain plugin;

    public PlayerInteractEntityListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void PlayerInteract(PlayerInteractEntityEvent event){
        Player player1 = event.getPlayer();
        Entity entity = event.getRightClicked();
        if (event.getPlayer().isSneaking()){
            if (event.getHand() == EquipmentSlot.HAND) {
                if (entity instanceof Player) {
                    //String name = ((Player) entity).getPlayer().getName();
                    Player player2 = (Player) entity;
                    MythicCharacter character = plugin.getCharacters().get(player2);
                    player1.sendMessage(ChatColor.GOLD + player2.getName() + "'s " + ChatColor.YELLOW + "Character Sheet");
                    player1.sendMessage(ChatColor.YELLOW + "Name: " + ChatColor.WHITE + character.getName());
                    player1.sendMessage(ChatColor.YELLOW + "Race: " + ChatColor.WHITE + character.getRace());
                    player1.sendMessage(ChatColor.YELLOW + "Gender: " + ChatColor.WHITE + character.getGender());
                    player1.sendMessage(ChatColor.YELLOW + "Professions: " + ChatColor.WHITE + character.getProfession());
                    if (player1.hasPermission("roll.info.op")) {
                        player1.sendMessage(ChatColor.YELLOW + "Alignment: " + ChatColor.WHITE + character.getAlignment());
                    }
                    if (player1.hasPermission("roll.info.op")) {
                        player1.sendMessage(ChatColor.YELLOW + "Languages: " + ChatColor.WHITE + character.getLang());
                    }
                    player1.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + character.getDescription());
                } else {
                    return;
                }
            }
        }
    }
}
