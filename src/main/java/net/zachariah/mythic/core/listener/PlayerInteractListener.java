package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

    private final MythicMain plugin;

    public PlayerInteractListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void PlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();
        Block block = event.getClickedBlock();
        if (action == Action.RIGHT_CLICK_BLOCK) {
            if (block.getState() instanceof Sign && block.getType() != null) {
                Sign sign = (Sign) block.getState();
                if (sign.getLine(0).equalsIgnoreCase(ChatColor.GOLD + "[Feed]")) {
                    player.setSaturation(10);
                    player.setFoodLevel(20);
                    player.sendMessage(ChatColor.YELLOW + "You eat a nice bowl of soup, you feel very satisfied!");
                }
            }
        }

    }
}
