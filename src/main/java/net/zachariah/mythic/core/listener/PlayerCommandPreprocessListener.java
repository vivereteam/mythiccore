package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.chat.MythicChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import net.zachariah.mythic.core.character.MythicCharacter;

import java.util.HashMap;

/**
 * Created by Zac on 2018-02-02.
 */
public class PlayerCommandPreprocessListener implements Listener {

    final MythicMain plugin;


    public PlayerCommandPreprocessListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    ScoreboardManager manager = Bukkit.getScoreboardManager();

    @EventHandler
    public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        String command = event.getMessage().split(" ")[0];
        for (String id : plugin.getChannels()){
            if (command.equalsIgnoreCase(("/" + id))){
                Player player = event.getPlayer();
                if (player.hasPermission("mythic.core." + id + ".send") && player.hasPermission("mythic.core." + id + ".read")){
                    event.setCancelled(true);
                    MythicChat channel = new MythicChat(plugin, id);
                    channel.send(player, event.getMessage().replace("/" + id + " ", ""));
                }
            }
        }
        HashMap<String, String> aliases = plugin.getChannelAliases();
        for (String alias : aliases.keySet()){
            if (command.equalsIgnoreCase(("/" + alias))){
                String id = aliases.get(alias);
                Player player = event.getPlayer();
                if (player.hasPermission("mythic.core." + id + ".send") && player.hasPermission("mythic.core." + id + ".read")){
                    event.setCancelled(true);
                    MythicChat channel = new MythicChat(plugin, id);
                    channel.send(player, event.getMessage().replace("/" + alias + " ", ""));
                }
            }
        }
    }




}
