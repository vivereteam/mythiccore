package net.zachariah.mythic.core.listener;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

public class EntityDamageListener implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (entity.hasMetadata("invincible")) {
            List<MetadataValue> values = entity.getMetadata("invincible");
            if (!values.isEmpty()) {
                values.forEach(value -> {
                    if (value.value() instanceof Boolean) {
                        if (value.asBoolean()) {
                            event.setCancelled(true);
                        }
                    }
                });
            }
        }
    }

}
