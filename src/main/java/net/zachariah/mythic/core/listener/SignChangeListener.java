package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

/**
 * Created by Zac on 2017-05-30.
 *
 * 50 nibs to a shard
 * 25 shards to a moon
 * 25 moons to a dragon
 * 25 dragons to a sun
 *
 */
public class SignChangeListener implements Listener {

    private final MythicMain plugin;

    public SignChangeListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        if (event.getLine(0).equalsIgnoreCase("[Feed]")) {
            if (event.getPlayer().hasPermission("roll.info.op")) {
                event.setLine(0, (ChatColor.GOLD + "[Feed]"));
            }
            else {
                event.setLine(0, (ChatColor.RED + "[Feed]"));
                event.setLine(1, (ChatColor.DARK_RED + "ERROR:No perms"));
                event.setLine(2, (ChatColor.GOLD + "(who do you)"));
                event.setLine(3, (ChatColor.GOLD + "(think you are.)"));
            }
        }
        else {
            return;
        }
    }

}
