package net.zachariah.mythic.core.listener;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.chat.MythicChat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scoreboard.ScoreboardManager;
import net.zachariah.mythic.core.character.MythicCharacter;

/**
 * Created by Zac on 2018-02-02.
 */
public class PlayerChatEventListener implements Listener {

    final MythicMain plugin;


    public PlayerChatEventListener(MythicMain plugin) {
        this.plugin = plugin;
    }

    ScoreboardManager manager = Bukkit.getScoreboardManager();

    @EventHandler
    public void PlayerChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        MythicCharacter character = plugin.getCharacters().get(player);
        event.setCancelled(true);

        if (character.getChannel() == null){
            character.setChannel(plugin.getChannels().get(0));
        }

        if (event.getMessage().startsWith("*")){
            MythicChat channel = new MythicChat(plugin, "me");
            channel.send(player, event.getMessage().substring(1));

        } else{
            MythicChat channel = new MythicChat(plugin, character.getChannel());
            channel.send(player, event.getMessage());
        }


    }




}
