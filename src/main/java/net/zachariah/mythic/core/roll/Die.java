package net.zachariah.mythic.core.roll;

import java.util.Random;

public class Die {

    private final int sides;
    private final Random random;

    public Die(int sides) {
        this.sides = sides;
        random = new Random();
    }

    public int roll() {
        return random.nextInt(sides) + 1;
    }

}
