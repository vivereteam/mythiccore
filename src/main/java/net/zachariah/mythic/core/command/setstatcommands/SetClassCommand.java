package net.zachariah.mythic.core.command.setstatcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetClassCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetClassCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            if (args.length >= 2) {
                Player player = plugin.getServer().getPlayer(args[0]);
                if (player != null) {
                    MythicCharacter character = plugin.getCharacters().get(player);
                    character.setClassName(args[1]);
                    sender.sendMessage(ChatColor.GREEN + player.getName() + "'s class has been set to " + args[1] + ".");
                } else {
                    sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
                }
            }
            else {
                sender.sendMessage(ChatColor.RED + "Not enough arguments. Use /setclass [player] [class]");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You don't have permission to do that!");
        }
        return true;
    }
}