package net.zachariah.mythic.core.command.charcardcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetNameCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetNameCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            MythicCharacter character = plugin.getCharacters().get(player);
            StringBuilder nameBuilder = new StringBuilder("");
            for (String arg : args) {
                nameBuilder.append(arg).append(" ");
            }
            nameBuilder.deleteCharAt(nameBuilder.length() - 1);
            if (nameBuilder.length() > plugin.getConfig().getInt("nick_max_length")){
                sender.sendMessage(ChatColor.RED + "Name can't be longer than " + plugin.getConfig().getInt("nick_max_length"));
                return true;
            }
            character.setName(nameBuilder.toString());
            player.setDisplayName(nameBuilder.toString());
            character.setNick(nameBuilder.toString());
            sender.sendMessage(ChatColor.GREEN + "Your name has been set");
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
        }

        /*plugin.getCharConfig().set("cards." + sender.getName() + ".name", "");
        StringBuilder d = new StringBuilder(plugin.getCharConfig().getString("cards." + sender.getName() + ".name"));
        for (String arg : args) {
            d.append(arg).append(" ");
        }
        plugin.getCharConfig().set("cards." + sender.getName() + ".name", d.toString());
        sender.sendMessage(ChatColor.GREEN + "Your name has been set.");
        plugin.saveCharConfig();*/
        return true;
    }

}
