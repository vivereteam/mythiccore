package net.zachariah.mythic.core.command.combatcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * Created by Zac on 2018-01-30.
 */
public class RollorderCommand implements CommandExecutor {

    private final MythicMain plugin;

    public RollorderCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    ScoreboardManager manager = Bukkit.getScoreboardManager();



    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player player = (Player) sender;

        Scoreboard rollorder = player.getScoreboard();

        if (args.length != 0) {
            if (sender instanceof Player) {
                if (args[0].equalsIgnoreCase("create")) {
                    if (sender.hasPermission("roll.info.op")) {
                        boolean contained = false;
                        if (player.getScoreboard().getTeam("NoCol").hasEntry(player.getName())){
                            contained = true;
                        }

                        player.setScoreboard(manager.getNewScoreboard());

                        rollorder = player.getScoreboard();

                        Team NoCol = rollorder.registerNewTeam("NoCol");

                        if (contained == true){
                            NoCol.addEntry(player.getName());
                        } else {
                            NoCol.addEntry(player.getName());
                        }

                        if (plugin.getConfig().get("VibeCheck").equals(true)){
                            NoCol.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.ALWAYS);
                        }
                        if (plugin.getConfig().get("VibeCheck").equals(false)){
                            NoCol.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
                        }

                        for(Player online : Bukkit.getServer().getOnlinePlayers()){
                            if (Bukkit.getServer().getOnlinePlayers() == null){
                                break;
                            }

                            NoCol = player.getScoreboard().getTeam("NoCol");

                            if((online.getScoreboard().getTeam("NoCol").hasEntry(online.getName()))){
                                NoCol.addEntry(online.getName());
                            } else {
                                NoCol.addEntry(online.getName());
                            }
                        }

                        Objective objective = rollorder.registerNewObjective("rollorder", "dummy");
                        objective.setDisplayName(ChatColor.GOLD + "Roll order:");

                        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

                        Score score = objective.getScore(ChatColor.GOLD + "EM: " + ChatColor.WHITE + player.getName());
                        score.setScore(50);

                        Score spacer = objective.getScore("");
                        spacer.setScore(49);

                    } else {
                        sender.sendMessage(ChatColor.RED + "You do not have permission to execute this command.");
                    }
                }

                if (args[0].equalsIgnoreCase("add")) {
                    if (sender.hasPermission("roll.info.op")) {
                        if (args[1] == null || args[2] == null) {
                            sender.sendMessage(ChatColor.RED + "/rollorder add (name) (initiative)");
                        } else {
                            Objective objective = rollorder.getObjective("rollorder");
                            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                            Score score = objective.getScore(ChatColor.YELLOW + args[1]);
                            score.setScore(Integer.parseInt(args[2]));
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "You do not have permission to execute that command.");
                    }
                }

                if (args[0].equalsIgnoreCase("show")) {
                    if (sender.hasPermission("roll.info.op")) {
                        if (args.length == 2) {
                            Player target = plugin.getServer().getPlayer(args[1]);

                            if (target != null) {
                                Objective objective = rollorder.getObjective("rollorder");
                                if (objective.getDisplaySlot() != null && objective != null){
                                    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                                    target.setScoreboard(player.getScoreboard());
                                } else {
                                    player.sendMessage(ChatColor.RED + "There is no roll order to show!");
                                }


                            } else {
                                sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
                            }
                        }
                        else if (args.length == 3){
                            Player recieve = plugin.getServer().getPlayer(args[1]);
                            Player take = plugin.getServer().getPlayer(args[2]);

                            if (take != null && recieve != null) {
                                Scoreboard takeorder = take.getScoreboard();

                                Objective objective = takeorder.getObjective("rollorder");
                                if (objective.getDisplaySlot() != null && objective != null){
                                    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                                    recieve.setScoreboard(take.getScoreboard());
                                } else {
                                    player.sendMessage(ChatColor.RED + "There is no roll order to show!");
                                }


                            } else {
                                sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
                            }
                        } else {
                            sender.sendMessage(ChatColor.RED + "/rollorder show [reciever] OR /rollorder show [reciever] [target]");
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "You do not have permission to execute that command.");
                    }
                }

                if (args[0].equalsIgnoreCase("hide")) {

                    player.setScoreboard(manager.getMainScoreboard());

//                    boolean contained = false;
//                    if (player.getScoreboard().getTeam("NoCol").hasEntry(player.getName())){
//                        contained = true;
//                    }
//
//                    player.setScoreboard(manager.getNewScoreboard());
//
//                    rollorder = player.getScoreboard();
//
//                    Team NoCol = rollorder.registerNewTeam("NoCol");
//
//                    if (contained == true){
//                        NoCol.addEntry(player.getName());
//                    } else {
//                        NoCol.addEntry(player.getName());
//                    }
//
//                    if (plugin.getConfig().get("VibeCheck").equals(true)){
//                        NoCol.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.ALWAYS);
//                    }
//                    if (plugin.getConfig().get("VibeCheck").equals(false)){
//                        NoCol.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
//                    }
//
//                    for(Player online : Bukkit.getServer().getOnlinePlayers()){
//                        if (Bukkit.getServer().getOnlinePlayers() == null){
//                            break;
//                        }
//
//                        NoCol = player.getScoreboard().getTeam("NoCol");
//
//                        if((online.getScoreboard().getTeam("NoCol").hasEntry(online.getName()))){
//                            NoCol.addEntry(online.getName());
//                        } else {
//                            NoCol.addEntry(online.getName());
//                        }
//                    }

                }


            } else {
                sender.sendMessage(ChatColor.RED + "This command may not be used from console.");
            }

        } else {
            sender.sendMessage(ChatColor.GOLD + "/rollorder [create/add/show/hide]");
        }

        return true;
    }
}