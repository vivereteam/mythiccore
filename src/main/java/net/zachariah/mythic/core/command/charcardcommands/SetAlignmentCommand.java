package net.zachariah.mythic.core.command.charcardcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Zac on 2017-06-29.
 */
public class SetAlignmentCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetAlignmentCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            MythicCharacter character = plugin.getCharacters().get(player);
            if (character.getAlignment() == null) {
                if (args.length > 0) {
                    character.setAlignment(args[0]);
                    sender.sendMessage(ChatColor.GREEN + "Your Alignment has been set.");
                } else {
                    sender.sendMessage(ChatColor.RED + "You must specify an Alignment.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Your Alignment has been locked. Please request for staff to reset your Alignment.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
        }
        return true;
    }
}


