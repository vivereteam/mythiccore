package net.zachariah.mythic.core.command.potioneffectcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Zac on 2017-04-30.
 */
public class LeapCommand implements CommandExecutor {

    private final MythicMain plugin;

    public LeapCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("vivere.pubbe")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (args.length != 0) {
                    if (args[0].equalsIgnoreCase("on")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1800000000, 4));
                        sender.sendMessage(ChatColor.GOLD + "You can now jump very high.");
                    } else if (args[0].equalsIgnoreCase("off")) {
                        player.removePotionEffect(PotionEffectType.JUMP);
                        sender.sendMessage(ChatColor.GOLD + "You can no longer jump really high.");
                    } else {
                        sender.sendMessage(ChatColor.RED + "You must specifiy on or off. EX: /leap [on/off]");
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "You must specifiy on or off. EX: /leap [on/off]");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You must be a player to preform this command.");
            }

        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
        }
        return true;
    }
}
