package net.zachariah.mythic.core.command.expcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ExpSetCommand implements CommandExecutor {

    private final MythicMain plugin;

    public ExpSetCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    public int getNewLevel(int currentEXP){
        int level = -1;
        for(int i = 1; i <= 20; i++){
            if (currentEXP >= (Integer) plugin.getConfig().get("level_table." + Integer.toString(i))){
                level = i;
            }
            else{
                return level;
            }
        }
        return level;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if (sender.hasPermission("roll.info.op")){
            if (args.length >= 2 ) {
                Player player = Bukkit.getServer().getPlayer(args[0]);
                MythicCharacter character = plugin.getCharacters().get(player);
                character.setStatValue(MythicStat.EXP, Integer.parseInt(args[1]));
                sender.sendMessage(ChatColor.GREEN + player.getName() + "'s exp has been set to " + args[1]);
                player.sendMessage(ChatColor.GREEN + "Your exp has been set to " + args[1]);

                int newTotal = character.getStatValue(MythicStat.EXP);

                player.setExp(0);

                int level = getNewLevel(newTotal);

                player.setLevel(level);
                character.setStatValue(MythicStat.LEVEL, level);
                if (level > character.getStatValue(MythicStat.LEVEL)) {
                    player.sendMessage(ChatColor.GOLD + "You are now level " + level + "!");
                    character.setStatValue(MythicStat.LEVEL, level);
                }

                if (level < 20) {
                    float lastCap = (Integer) plugin.getConfig().get("level_table." + Integer.toString(level));
                    float ourCap = (Integer) plugin.getConfig().get("level_table." + Integer.toString(level + 1)) - lastCap;
                    float percentTo = ((newTotal - lastCap) / ourCap);
                    player.setExp(percentTo);
                }

            } else {
                sender.sendMessage(ChatColor.RED + "Not enough arguments. Use /setexp [player] [exp]");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to do that!");
        }
        return true;
    }


}
