package net.zachariah.mythic.core.command.memecommands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created by Zac on 2018-02-04.
 */
public class ZugguratCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.YELLOW + "* A gnoll screeches from the jungle *");
        return true;
    }
}
