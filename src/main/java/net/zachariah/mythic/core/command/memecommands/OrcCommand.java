package net.zachariah.mythic.core.command.memecommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.metadata.LazyMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class OrcCommand implements CommandExecutor {

    private MythicMain plugin;

    public OrcCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform that command.");
            return true;
        }
        Player player = ((Player) sender);
        if (plugin.getCharacters().get(player).isOrc()){
            player.sendMessage(ChatColor.GOLD + "Brandon sends his regards.");
            return true;
        }
        plugin.getCharacters().get(player).setIsOrc(true);
        player.getWorld().getPlayers()
                .stream()
                .filter(other -> other.getLocation().distanceSquared(player.getLocation()) <= 1024)
                .forEach(other -> other.sendMessage(ChatColor.RED + player.getDisplayName() + ": OOGA BOOGA"));
        Entity golem = player.getWorld().spawnEntity(player.getLocation(), EntityType.IRON_GOLEM);
        golem.setMetadata("invincible", new LazyMetadataValue(plugin, () -> true));
        golem.setPassenger(player);
        BukkitTask task = new BukkitRunnable() {
            @Override
            public void run() {
                Location adjustedLocation = new Location(
                        golem.getLocation().getWorld(),
                        golem.getLocation().getX(),
                        golem.getLocation().getY(),
                        golem.getLocation().getZ(),
                        golem.getLocation().getYaw(),
                        golem.getLocation().getPitch() + 10
                );

                golem.teleport(adjustedLocation);
            }
        }.runTaskTimer(plugin, 10L, 10L);
        new BukkitRunnable() {
            @Override
            public void run() {
                task.cancel();
                golem.eject();
                golem.remove();
                plugin.getCharacters().get(player).setIsOrc(false);
            }
        }.runTaskLater(plugin, 200L);
        return true;
    }
}
