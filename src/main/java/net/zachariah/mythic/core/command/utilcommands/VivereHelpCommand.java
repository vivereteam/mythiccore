package net.zachariah.mythic.core.command.utilcommands;


import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created by Zac on 2017-07-09.
 * Edited by Nym on 2017-07-11.
 */
public class VivereHelpCommand implements CommandExecutor {

    private final MythicMain plugin;

    public VivereHelpCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
        if (args.length == 0){
            sender.sendMessage(ChatColor.GOLD + "[VivereHelp 1/7]");
            sender.sendMessage(ChatColor.YELLOW + "/char - Displays personal character card commands");
            sender.sendMessage(ChatColor.YELLOW + "/char [playername] - Displays [playername]'s character card");
            sender.sendMessage(ChatColor.YELLOW + "/rollinfo - Staff can add [playername]");
            sender.sendMessage(ChatColor.YELLOW + "/mine - Profession must be Miner or Spelunker");
            sender.sendMessage(ChatColor.YELLOW + "/hp - Displays current hp");
            sender.sendMessage(ChatColor.YELLOW + "/hhp [value] - Heals for [value] Hp");
            sender.sendMessage(ChatColor.YELLOW + "/dhp [value] - Damages for [value] Hp");
            sender.sendMessage(ChatColor.YELLOW + "/restorehp - Fully restores Hp");
            sender.sendMessage(ChatColor.YELLOW + "/mana - Displays current mana");
            sender.sendMessage(ChatColor.YELLOW + "/gmana [value] - Adds [value] mana to current pool");

        }
        else if (args.length == 1){
            if (args[0] == "1"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 1/7]");
                sender.sendMessage(ChatColor.YELLOW + "/char - Displays personal character card commands");
                sender.sendMessage(ChatColor.YELLOW + "/char [playername] - Displays [playername]'s character card");
                sender.sendMessage(ChatColor.YELLOW + "/rollinfo - Staff can add [playername]");
                sender.sendMessage(ChatColor.YELLOW + "/mine - Profession must be Miner or Spelunker");
                sender.sendMessage(ChatColor.YELLOW + "/hp - Displays current hp");
                sender.sendMessage(ChatColor.YELLOW + "/hhp [value] - Heals for [value] Hp");
                sender.sendMessage(ChatColor.YELLOW + "/dhp [value] - Damages for [value] Hp");
                sender.sendMessage(ChatColor.YELLOW + "/restorehp - Fully restores Hp");
                sender.sendMessage(ChatColor.YELLOW + "/mana - Displays current mana");
                sender.sendMessage(ChatColor.YELLOW + "/gmana [value] - Adds [value] mana to current pool");
            }
            else if (args[0] == "2"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 2/7]");
                sender.sendMessage(ChatColor.YELLOW + "/lmana [value] - Subtracts [value] mana from current pool");
                sender.sendMessage(ChatColor.YELLOW + "/restoremana - Fully restores mana pool");
                sender.sendMessage(ChatColor.YELLOW + "/con - Displays concentration pool if applicable");
                sender.sendMessage(ChatColor.YELLOW + "/gcon [value] - Adds [value] concentration to current pool");
                sender.sendMessage(ChatColor.YELLOW + "/lcon [value] - Subtracts [value] concentration from current pool");
                sender.sendMessage(ChatColor.YELLOW + "/restorecon - Fully restores concentration pool");
                sender.sendMessage(ChatColor.YELLOW + "/pan - Displays panache pool if applicable");
                sender.sendMessage(ChatColor.YELLOW + "/gpan [value] - Adds [value] panache to current pool");
                sender.sendMessage(ChatColor.YELLOW + "/lpan [value] - Subtracts [value] panache from current pool");
                sender.sendMessage(ChatColor.YELLOW + "/restorepan - Fully restores panache pool");

            }
            else if (args[0] == "3"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 3/7]");
                sender.sendMessage(ChatColor.YELLOW + "/roll [die] - Rolls dice in format of XdY(+-Z)");
                sender.sendMessage(ChatColor.YELLOW + "/feats - Displays current feats. [Playername] may be added if staff");
                sender.sendMessage(ChatColor.YELLOW + "/setfeat [feat] - If available, sets [feat] to your feats table");
                sender.sendMessage(ChatColor.YELLOW + "/nightvision [on/off] - Need vivere.nightvision");
                sender.sendMessage(ChatColor.YELLOW + "/aqualung [on/off] - Need vivere.waterbreathing");
                sender.sendMessage(ChatColor.YELLOW + "/leap [on/off] - Need vivere.pubbe");
                sender.sendMessage(ChatColor.YELLOW + "/chp [playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/cmana [Playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/ccon [playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/cpan [playername] - Staff");
            }
            else if (args[0] == "4"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 4/7]");
                sender.sendMessage(ChatColor.YELLOW + "/sethp [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setmana [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setcon [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setpan [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setmr [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setec [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setac [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setwill [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setstr [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setdex [playername] [value] - Staff");
            }
            else if (args[0] == "5"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 5/7]");
                sender.sendMessage(ChatColor.YELLOW + "/setfeatslots [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setmrmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setecmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setacmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setwillmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setstrmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setdexmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/sethpmod [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/setclass [playername] [value] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/levelup [playername] [value] - Staff");
            }
            else if (args[0] == "6"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 6/7]");
                sender.sendMessage(ChatColor.YELLOW + "/resetfeat [tier#] [feat#] [playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/resetfeats [playername - Staff]");
                sender.sendMessage(ChatColor.YELLOW + "/staffsetfeat [playername] [tier#] [feat#] [feat] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/freeze [playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/unfreeze [playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/gather [herbalist/hunter] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/resetpro [playermame] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/resetalign [playername] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/addore [worldname] [X] [Y] [Z] [orename] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/addveins [worldname] [X] [Y] [Z] [veinamount] - Staff");
            }
            else if (args[0] == "7"){
                sender.sendMessage(ChatColor.GOLD + "[VivereHelp 7/7]");
                sender.sendMessage(ChatColor.YELLOW + "/editvein1 [worldname] [X] [Y] [Z] [veinamount] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/editvein2 [worldname] [X] [Y] [Z] [veinamount] - Staff");
                sender.sendMessage(ChatColor.YELLOW + "/editvein3 [worldname] [X] [Y] [Z] [veinamount] - Staff");
            }
        } return true;
    }
}
