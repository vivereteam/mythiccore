package net.zachariah.mythic.core.command.expcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ExpGiveCommand implements CommandExecutor {

    private final MythicMain plugin;

    public ExpGiveCommand(MythicMain plugin) {
        this.plugin = plugin;
    }
    // /giveexp [player] [exp(int)]
    // Player must be online
    // Check if exp > exp_cap
    //
    public int getNewLevel(int currentEXP){
        int level = -1;
        for(int i = 1; i <= 20; i++){
            if (currentEXP >= (Integer) plugin.getConfig().get("level_table." + Integer.toString(i))){
                level = i;
            }
            else{
                return level;
            }
        }
        return level;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
        if (args.length >= 2){
            // Enough Arguments
            Player player = Bukkit.getServer().getPlayer(args[0]);
            if (player != null){
                MythicCharacter character = plugin.getCharacters().get(player);
                //Player does exist
                int exp = Integer.parseInt(args[1]);

                if (exp < 0){
                    sender.sendMessage(ChatColor.RED + "EXP must be a positive integer.");
                    return true;
                }

                int current = character.getStatValue(MythicStat.EXP);
                character.setStatValue(MythicStat.EXP, exp + current);
                sender.sendMessage(ChatColor.GREEN + player.getName() +  " has gained " + exp + " exp!");
                player.sendMessage(ChatColor.GREEN + "You gained " + exp + " exp!");
                int newTotal = character.getStatValue(MythicStat.EXP);

                player.setExp(0);

                int level = getNewLevel(newTotal);

//                if (level == -1){
//                    return true;
//                }
                player.setLevel(level);
                if (level > character.getStatValue(MythicStat.LEVEL)) {
                    player.sendMessage(ChatColor.GOLD + "You are now level " + level + "!");
                    character.setStatValue(MythicStat.LEVEL, level);
                }

                if (level < 20) {
                    float mcCap = player.getExpToLevel();
                    float lastCap = (Integer) plugin.getConfig().get("level_table." + Integer.toString(level));
                    float ourCap = (Integer) plugin.getConfig().get("level_table." + Integer.toString(level + 1)) - lastCap;
                    float percentTo = ((newTotal - lastCap) / ourCap);
                    player.setExp(percentTo);
                }

//                sender.sendMessage(ChatColor.GREEN + "Success! " + player.getName() + " has gained " + exp + " exp.");
//
            }
            else{
                sender.sendMessage(ChatColor.RED + "Error: Player must be online.");
            }
        }
        else{
            sender.sendMessage(ChatColor.RED + "Not enough Arguments. /addexp <player> <exp_amount>");
        }
        return true;
    }

}
