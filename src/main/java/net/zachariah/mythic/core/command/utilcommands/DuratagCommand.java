package net.zachariah.mythic.core.command.utilcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Zac on 2018-02-03.
 */
public class DuratagCommand implements CommandExecutor {

    private final MythicMain plugin;

    public DuratagCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Cant use from console.");
                return true;
            }
            Player p = Bukkit.getServer().getPlayer(sender.getName());
            if (p.hasPermission("vivere.duratag")) {
                ItemStack itemSave = new ItemStack(p.getInventory().getItemInMainHand());
                if (p.getInventory().getItemInMainHand().getType() != Material.AIR) {
                    ItemMeta meta = itemSave.getItemMeta();
                    meta.setUnbreakable(true);
                    meta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_PLACED_ON, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_UNBREAKABLE});
                    itemSave.setItemMeta(meta);
                    p.getInventory().setItemInMainHand(itemSave);
                    sender.sendMessage(ChatColor.GREEN + "Item state saved!");
                    return true;
                }
                sender.sendMessage(ChatColor.RED + "Can't duratag air!");
                return true;
            }
            sender.sendMessage(ChatColor.RED + "You do not have the permission 'vivere.duratag'. Contact an administrator.");
            return true;

    }
}
