package net.zachariah.mythic.core.command.utilcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Zac on 2018-02-03.
 */
public class UnstuckCommand implements CommandExecutor {

    private final MythicMain plugin;

    public UnstuckCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
        if (sender instanceof Player){
            Player player = (Player) sender;
            Material block = player.getLocation().getBlock().getRelative(0,-1,0).getType();
            double X = player.getLocation().getX();
            double Y = (player.getLocation().getY()) + 4;
            double Z = player.getLocation().getZ();
            float P = player.getLocation().getPitch();
            float YAW = player.getLocation().getYaw();
            Location location = new Location(player.getWorld(), X, Y, Z, YAW, P);
            if (block != Material.AIR){
                player.teleport(location);
                player.sendMessage("WHOOSH!");
            }
            else{
                player.sendMessage("What do you think this is? /orc?");
            }
        } else{
            sender.sendMessage(ChatColor.RED + "You must be a player to execute this command.");
        }
        return true;
    }
}
