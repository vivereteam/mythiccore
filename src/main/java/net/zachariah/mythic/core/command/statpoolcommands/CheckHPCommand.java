package net.zachariah.mythic.core.command.statpoolcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CheckHPCommand implements CommandExecutor {

    private final MythicMain plugin;

    public CheckHPCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            if (args[0] != null) {
                Player player = plugin.getServer().getPlayer(args[0]);
                if (player != null) {
                    MythicCharacter character = plugin.getCharacters().get(player);
                    if (character.getStatValue(MythicStat.TEMP_HP) > 0){
                        sender.sendMessage(ChatColor.YELLOW + player.getName() + "'s current HP is " + ChatColor.AQUA + "(" +
                                character.getStatValue(MythicStat.TEMP_HP) + ") " + ChatColor.YELLOW
                                + character.getStatValue(MythicStat.CURRENT_HP));
                    }
                    else {
                        sender.sendMessage(ChatColor.YELLOW + player.getName() + "'s  current HP is " + character.getStatValue(MythicStat.CURRENT_HP));
                    }
                }
                else {
                sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Error: Must specify a player. EX: /chp (playername)");
            }

        } else {
            sender.sendMessage(ChatColor.RED + "You don't have permission to issue this Command.");
        }
        return true;
    }
}
