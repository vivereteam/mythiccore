package net.zachariah.mythic.core.command.staffutilcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class UnfreezeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "effect " + args[0] + " clear");
            sender.sendMessage(ChatColor.GOLD + args[0] + ChatColor.YELLOW + " has been unfrozen");
        }
        return true;
    }

}
