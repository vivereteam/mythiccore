package net.zachariah.mythic.core.command.chatcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteCommand implements CommandExecutor {

    private final MythicMain plugin;

    public MuteCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length > 0) {
                Player player = (Player) sender;
                MythicCharacter character = plugin.getCharacters().get(player);
                String channel = args[0];
                if (player.hasPermission("mythic.core." + channel + ".read")){
                    if (character.hasChannelMuted(channel)){
                        character.setMute(channel, false);
                        sender.sendMessage(ChatColor.GOLD + channel + " has been un-muted.");
                    }
                    else if (!character.hasChannelMuted(channel)){
                        character.setMute(channel, true);
                        sender.sendMessage(ChatColor.GOLD + channel + " has been muted.");
                    }
                    else {
                        sender.sendMessage(ChatColor.RED + "ERROR: ChannelMuteError. Report to Administrator.");
                    }

                } else {
                    sender.sendMessage(ChatColor.RED + "That is not a valid channel.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Not enough Arguments. /mute {channel}");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Only a player can execute this command.");
        }

        return true;
    }
}