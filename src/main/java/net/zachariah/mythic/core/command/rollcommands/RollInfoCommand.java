package net.zachariah.mythic.core.command.rollcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RollInfoCommand implements CommandExecutor {

    private final MythicMain plugin;

    public RollInfoCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        OfflinePlayer player = null;
        if (args.length > 0) {
            if (sender.hasPermission("roll.info.op")) {
                player = plugin.getServer().getOfflinePlayer(args[0]);
            } else {
                sender.sendMessage(ChatColor.RED + "You do not have permission to see the stats of other players!");
            }
        }
        if (player == null) {
            if (sender instanceof Player) {
                player = (Player) sender;
            }
        }
        if (player == null) {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
            return true;
        }
        MythicCharacter character = plugin.getCharacters().get(player);
        sender.sendMessage(ChatColor.GOLD + character.getName() + " is a Level " + character.getStatValue(MythicStat.LEVEL) + " " + character.getClassName());
        sender.sendMessage(ChatColor.YELLOW + "HP: " + ChatColor.RESET + character.getStatValue(MythicStat.HP));
        sender.sendMessage(ChatColor.YELLOW + "Strength: " + ChatColor.RESET + character.getStatValue(MythicStat.STRENGTH) + ChatColor.AQUA + " (" + character.getStatValue(MythicStat.STRENGTH_MOD) + ")");
        sender.sendMessage(ChatColor.YELLOW + "Dexterity: " + ChatColor.RESET + character.getStatValue(MythicStat.DEXTERITY) + ChatColor.AQUA + " (" + character.getStatValue(MythicStat.DEXTERITY_MOD) + ")");
        sender.sendMessage(ChatColor.YELLOW + "Constitution: " + ChatColor.RESET + character.getStatValue(MythicStat.CONSTITUTION) + ChatColor.AQUA + " (" + character.getStatValue(MythicStat.CONSTITUTION_MOD) + ")");
        sender.sendMessage(ChatColor.YELLOW + "Intelligence: " + ChatColor.RESET + character.getStatValue(MythicStat.INTELLIGENCE) + ChatColor.AQUA + " (" + character.getStatValue(MythicStat.INTELLIGENCE_MOD) + ")");
        sender.sendMessage(ChatColor.YELLOW + "Wisdom: " + ChatColor.RESET + character.getStatValue(MythicStat.WISDOM) + ChatColor.AQUA + " (" + character.getStatValue(MythicStat.WISDOM_MOD) + ")");
        sender.sendMessage(ChatColor.YELLOW + "Charisma: " + ChatColor.RESET + character.getStatValue(MythicStat.CHARISMA) + ChatColor.AQUA + " (" + character.getStatValue(MythicStat.CHARISMA_MOD) + ")");
        return true;
    }
}
