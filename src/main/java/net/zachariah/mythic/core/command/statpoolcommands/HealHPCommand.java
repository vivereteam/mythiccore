package net.zachariah.mythic.core.command.statpoolcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HealHPCommand implements CommandExecutor {

    private final MythicMain plugin;

    public HealHPCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            MythicCharacter character = plugin.getCharacters().get(player);
            if (args.length > 0) {
                int currentHp = character.getStatValue(MythicStat.CURRENT_HP);
                int hpHeal = Integer.parseInt(args[0]);
                character.setStatValue(MythicStat.CURRENT_HP, currentHp + hpHeal);
                sender.sendMessage(ChatColor.GREEN + "You have gained " + hpHeal + " HP!");
                if (character.getStatValue(MythicStat.CURRENT_HP) > character.getStatValue(MythicStat.HP)) {
                    character.setStatValue(MythicStat.CURRENT_HP, character.getStatValue(MythicStat.HP));
                }
                player.getNearbyEntities(15.0D, 15.0D, 15.0D)
                        .stream()
                        .filter(entity -> entity instanceof Player)
                        .map(entity -> (Player) entity)
                        .forEach(otherPlayer -> otherPlayer.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " has gained " + args[0] + " HP!"));
            } else {
                sender.sendMessage(ChatColor.RED + "You must specify the amount of hp to regenerate.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
        }
        return true;
    }
}
