package net.zachariah.mythic.core.command.chatcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class ChannelCommand implements CommandExecutor {

    private final MythicMain plugin;

    public ChannelCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player player = (Player) sender;
            if (args.length > 0){
                for (String channel : plugin.getChannels()){
                    if (args[0].equalsIgnoreCase(channel) && player.hasPermission("mythic.core." + channel + ".focus")){
                        MythicCharacter character = plugin.getCharacters().get(player);
                        character.setChannel(channel);
                        sender.sendMessage(ChatColor.GREEN + "Channel set to " + channel);
                        return true;
                    }
                }
                HashMap<String, String> aliases = plugin.getChannelAliases();
                for (String alias : aliases.keySet()){
                    if (args[0].equalsIgnoreCase(alias)){
                        String channel = aliases.get(alias);
                        if (player.hasPermission("mythic.core." + channel + ".focus")){
                            MythicCharacter character = plugin.getCharacters().get(player);
                            character.setChannel(channel);
                            sender.sendMessage(ChatColor.GREEN + "Channel set to " + channel);
                            return true;
                        }

                    }
                }
                sender.sendMessage(ChatColor.RED + "That is not a valid channel.");

            }
            else {
                sender.sendMessage(ChatColor.RED + "Not enough arguments. /ch {channel}");
            }
        }
        else {
            sender.sendMessage(ChatColor.RED + "Only a player can execute this command.");
        }
        return true;

    }

}
