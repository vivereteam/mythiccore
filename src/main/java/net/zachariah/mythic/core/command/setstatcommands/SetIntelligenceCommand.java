package net.zachariah.mythic.core.command.setstatcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetIntelligenceCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetIntelligenceCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            Player player = plugin.getServer().getPlayer(args[0]);
            if (player != null) {
                MythicCharacter character = plugin.getCharacters().get(player);
                MythicStat stat = MythicStat.INTELLIGENCE;
                MythicStat mod = MythicStat.INTELLIGENCE_MOD;
                character.setStatValue(stat, Integer.parseInt(args[1]));
                sender.sendMessage(ChatColor.YELLOW + args[0] + "'s Intelligence has been set to " + args[1] + ".");
                for (int i=8; i < 21; i++) {
                    if (character.getStatValue(stat) == 8 && 8 == i || character.getStatValue(stat) == 9 && 9 == i) {
                        character.setStatValue(mod, -1);
                    } else if (character.getStatValue(stat) == 10 && 10 == i || character.getStatValue(stat) == 11 && 11 == i) {
                        character.setStatValue(mod, 0);
                    } else if (character.getStatValue(stat) == 12 && 12 == i || character.getStatValue(stat) == 13 && 13 == i) {
                        character.setStatValue(mod, 1);
                    } else if (character.getStatValue(stat) == 14 && 14 == i || character.getStatValue(stat) == 15 && 15 == i) {
                        character.setStatValue(mod, 2);
                    } else if (character.getStatValue(stat) == 16 && 16 == i || character.getStatValue(stat) == 17 && 17 == i) {
                        character.setStatValue(mod, 3);
                    } else if (character.getStatValue(stat) == 18 && 18 == i || character.getStatValue(stat) == 19 && 19 == i) {
                        character.setStatValue(mod, 4);
                    } else if (character.getStatValue(stat) == 20 && 20 == i) {
                        character.setStatValue(mod, 5);
                    }
                }
            } else {
                sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You don't have permission to set Stats!");
        }
        return true;
    }
}
