package net.zachariah.mythic.core.command.potioneffectcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Zac on 2017-04-30.
 */
public class WaterbreathingCommand implements CommandExecutor {

    private final MythicMain plugin;

    public WaterbreathingCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("vivere.waterbreathing")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (args.length != 0) {
                    if (args[0].equalsIgnoreCase("on")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 1800000000, 1000));
                        sender.sendMessage(ChatColor.GOLD + "Your lungs are now able to breathe water.");
                    } else if (args[0].equalsIgnoreCase("off")) {
                        player.removePotionEffect(PotionEffectType.WATER_BREATHING);
                        sender.sendMessage(ChatColor.GOLD + "You decide to stop breathing water.");
                    } else {
                        sender.sendMessage(ChatColor.RED + "You must specifiy on or off. EX: /aqualung [on/off]");
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "You must specifiy on or off. EX: /aqualung [on/off]");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You must be a player to preform this command.");
            }

        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
        }
        return true;
    }
}
