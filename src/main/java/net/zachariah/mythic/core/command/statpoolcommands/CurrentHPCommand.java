package net.zachariah.mythic.core.command.statpoolcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CurrentHPCommand implements CommandExecutor {

    private final MythicMain plugin;

    public CurrentHPCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            MythicCharacter character = plugin.getCharacters().get(player);
            if (character.getStatValue(MythicStat.TEMP_HP) > 0){
                sender.sendMessage(ChatColor.YELLOW + "Your current HP is " + ChatColor.AQUA + "(" +
                        character.getStatValue(MythicStat.TEMP_HP) + ") " + ChatColor.YELLOW
                        + character.getStatValue(MythicStat.CURRENT_HP));
            }
            else {
                sender.sendMessage(ChatColor.YELLOW + "Your current HP is " + character.getStatValue(MythicStat.CURRENT_HP));
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
        }
        return true;
    }
}
