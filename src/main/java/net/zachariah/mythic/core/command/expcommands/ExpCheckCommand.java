package net.zachariah.mythic.core.command.expcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ExpCheckCommand implements CommandExecutor {

    private final MythicMain plugin;

    public ExpCheckCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if (args.length > 0 && sender.hasPermission("roll.info.op")){
            Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
            MythicCharacter targetChar = plugin.getCharacters().get(targetPlayer);
            if (targetChar.getStatValue(MythicStat.LEVEL) == 20){
                sender.sendMessage(ChatColor.GOLD + targetChar.getName() + " is max level!");
            }
            else{
                sender.sendMessage(ChatColor.GOLD + targetPlayer.getName() + " has "+ targetChar.getStatValue(MythicStat.EXP) + "/" +
                        plugin.getConfig().get("level_table." + (targetChar.getStatValue(MythicStat.LEVEL) + 1)) + " exp.");
            }


        } else {
            Player targetPlayer = Bukkit.getServer().getPlayer(sender.getName());
            MythicCharacter targetChar = plugin.getCharacters().get(targetPlayer);
            if (targetChar.getStatValue(MythicStat.LEVEL) == 20){
                sender.sendMessage(ChatColor.GOLD + "You are max level!");
            }
            else{
                sender.sendMessage(ChatColor.GOLD + "You have " + targetChar.getStatValue(MythicStat.EXP) + "/" +
                        plugin.getConfig().get("level_table." + (targetChar.getStatValue(MythicStat.LEVEL) + 1)) + " exp.");
            }

        }
        return true;
    }




}
