package net.zachariah.mythic.core.command.statpoolcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DamageHPCommand implements CommandExecutor {

    private final MythicMain plugin;

    public DamageHPCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length > 0) {
                Player player = (Player) sender;
                MythicCharacter character = plugin.getCharacters().get(player);
                int currenthp = character.getStatValue(MythicStat.CURRENT_HP);
                int temphp = character.getStatValue(MythicStat.TEMP_HP);
                try {
                    int hpremove = Integer.parseInt(args[0]);
                    final int hpremoveStr = Integer.parseInt(args[0]);

                    sender.sendMessage(ChatColor.RED + "You have taken " + hpremove + " points of damage.");

                    if (temphp > 0){
                        character.setStatValue(MythicStat.TEMP_HP, temphp - hpremove);
                        if (hpremove > temphp){
                            hpremove = hpremove - temphp;
                        }
                        else {
                            hpremove = 0;
                        }
                    }
                    if (hpremove > 0){
                        character.setStatValue(MythicStat.CURRENT_HP, currenthp - hpremove);
                    }
                    if (character.getStatValue(MythicStat.CURRENT_HP) <= 0) {
                        sender.sendMessage(ChatColor.RED + "Your HP has reached or fallen below 0...");
                        character.setStatValue(MythicStat.CURRENT_HP, 0);
                    }

                    player.getNearbyEntities(15.0D, 15.0D, 15.0D)
                            .stream()
                            .filter(entity -> entity instanceof Player)
                            .map(entity -> (Player) entity)
                            .forEach(otherPlayer -> {
                                otherPlayer.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.RED + " has taken " + hpremoveStr + " points of damage.");
                                if (character.getStatValue(MythicStat.CURRENT_HP) <= 0) {
                                    otherPlayer.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.RED + "'s HP has reached or fallen below 0...");
                                }
                            });


                } catch (NumberFormatException exception) {
                    sender.sendMessage(ChatColor.RED + "The amount of HP to remove must be an integer.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You must specify the amount of HP to receive in damage.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
        }
        return true;
    }
}
