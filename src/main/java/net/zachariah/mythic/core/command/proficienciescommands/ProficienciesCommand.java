package net.zachariah.mythic.core.command.proficienciescommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Zac on 2017-11-15.
 */
public class ProficienciesCommand  implements CommandExecutor {

    private final MythicMain plugin;

    public ProficienciesCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        OfflinePlayer player = null;
        if (args.length > 0) {
            if (sender.hasPermission("roll.info.op")) {
                player = plugin.getServer().getOfflinePlayer(args[0]);
            } else {
                sender.sendMessage(ChatColor.RED + "You do not have permission to see the proficiencies of other players!");
            }
        }
        if (player == null) {
            if (sender instanceof Player) {
                player = (Player) sender;
            }
        }
        if (player == null) {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
            return true;
        }
        MythicCharacter character = plugin.getCharacters().get(player);
        sender.sendMessage(ChatColor.GOLD + character.getName() + "'s proficiencies:");
        sender.sendMessage(ChatColor.YELLOW + "Weapon: " + ChatColor.RESET + character.getWeaponProf());
        sender.sendMessage(ChatColor.YELLOW + "Armor: " + ChatColor.RESET + character.getArmorProf());
        sender.sendMessage(ChatColor.YELLOW + "Tool: " + ChatColor.RESET + character.getToolProf());
        sender.sendMessage(ChatColor.YELLOW + "Skills: " + ChatColor.RESET + character.getSkillProf());
        return true;
    }
}