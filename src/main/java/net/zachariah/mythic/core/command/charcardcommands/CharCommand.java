package net.zachariah.mythic.core.command.charcardcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CharCommand implements CommandExecutor {

	private final MythicMain plugin;

	public CharCommand(MythicMain plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	//GetOfflinePlayer is utilized as a (theoretically) safer way than guessing a UUID
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			if (args.length == 0 || args[0] == null) {
				sender.sendMessage(ChatColor.GOLD + "Character Card Commands:");
				sender.sendMessage(ChatColor.YELLOW + "/setname [Name] - Sets visible character card name");
				sender.sendMessage(ChatColor.YELLOW + "/setrace [Race] - Sets visible character card race");
				sender.sendMessage(ChatColor.YELLOW + "/setgen [Gender] - Sets visible character card gender");
				sender.sendMessage(ChatColor.YELLOW + "/setpro [Profession(s)]- Sets professions, cannot be undone");
				sender.sendMessage(ChatColor.YELLOW + "/setdes [Full Description] - Sets character card description");
				sender.sendMessage(ChatColor.YELLOW + "/adddes [Additional Text] - Adds to current card description");
				sender.sendMessage(ChatColor.YELLOW + "/setalignment [Alignment] - Sets visible character card alignment");
			} else {
				OfflinePlayer player = Bukkit.getServer().getOfflinePlayer(args[0]);
				MythicCharacter character = plugin.getCharacters().get(player);
				sender.sendMessage(ChatColor.GOLD + player.getName() + "'s " + ChatColor.YELLOW + "Character Sheet");
				sender.sendMessage(ChatColor.YELLOW + "Name: " + ChatColor.WHITE + character.getName());
				sender.sendMessage(ChatColor.YELLOW + "Race: " + ChatColor.WHITE + character.getRace());
				sender.sendMessage(ChatColor.YELLOW + "Gender: " + ChatColor.WHITE + character.getGender());
				sender.sendMessage(ChatColor.YELLOW + "Professions: " + ChatColor.WHITE + character.getProfession());
				if (sender.hasPermission("roll.info.op")) {
					sender.sendMessage(ChatColor.YELLOW + "Alignment: " + ChatColor.WHITE + character.getAlignment());
				} else if (sender.getName() == player.getName()) {
					sender.sendMessage(ChatColor.YELLOW + "Alignment: " + ChatColor.WHITE + character.getAlignment());
				}
				if (sender.hasPermission("roll.info.op")) {
					sender.sendMessage(ChatColor.YELLOW + "Languages: " + ChatColor.WHITE + character.getLang());
				} else if (sender.getName() == player.getName()) {
					sender.sendMessage(ChatColor.YELLOW + "Languages: " + ChatColor.WHITE + character.getLang());
				}

				sender.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + character.getDescription());

			}
		} else{
			sender.sendMessage("Command must be accessed ingame.");
			return true;
		}
		return true;
	}
}
