package net.zachariah.mythic.core.command.memecommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RigCommand implements CommandExecutor {

    private MythicMain plugin;

    public RigCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        sender.sendMessage(ChatColor.GOLD + "DICE RIGGED BOSS!");
        if (sender instanceof Player) {
            plugin.getRigger().setRigging((Player) sender, true);
        }
        return true;
    }
}
