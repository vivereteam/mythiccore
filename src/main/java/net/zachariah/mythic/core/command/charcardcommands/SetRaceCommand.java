package net.zachariah.mythic.core.command.charcardcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetRaceCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetRaceCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
            return true;
        }
        Player player = (Player) sender;
        MythicCharacter character = plugin.getCharacters().get(player);
        StringBuilder raceBuilder = new StringBuilder();
        for (String arg : args) {
            raceBuilder.append(arg).append(' ');
        }
        character.setRace(raceBuilder.toString().substring(0, raceBuilder.length() - 1));
        sender.sendMessage(ChatColor.GREEN + "Your race has been set.");
        return true;
    }
}
