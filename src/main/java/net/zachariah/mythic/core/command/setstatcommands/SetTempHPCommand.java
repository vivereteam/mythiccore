package net.zachariah.mythic.core.command.setstatcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetTempHPCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetTempHPCommand(MythicMain plugin) {
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length >= 1) {
            Player player = plugin.getServer().getPlayer(sender.getName());
            if (player != null) {
                MythicCharacter character = plugin.getCharacters().get(player);
                character.setStatValue(MythicStat.TEMP_HP, Integer.parseInt(args[0]));
                sender.sendMessage(ChatColor.YELLOW + sender.getName() + "'s Temp HP has been set to " + args[0] + ".");

                player.getNearbyEntities(15.0D, 15.0D, 15.0D)
                        .stream()
                        .filter(entity -> entity instanceof Player)
                        .map(entity -> (Player) entity)
                        .forEach(otherPlayer -> {
                            otherPlayer.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " has " + character.getStatValue(MythicStat.TEMP_HP) + " temporary hit points.");
                        });

            } else {
                sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Not enough arguments. Use /temphp [hp]");
        }
    return true;
    }
}
