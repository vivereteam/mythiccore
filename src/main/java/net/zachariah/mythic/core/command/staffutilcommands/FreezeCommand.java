package net.zachariah.mythic.core.command.staffutilcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class FreezeCommand implements CommandExecutor {

    private final MythicMain plugin;

    public FreezeCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            Player player = plugin.getServer().getPlayer(args[0]);
            if (player != null) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1800000000, 1000));
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1800000000, 200));
                sender.sendMessage(ChatColor.GOLD + args[0] + ChatColor.YELLOW + " has been frozen");
            } else {
                sender.sendMessage(ChatColor.RED + "There is no player by that name online.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission.");
        }
        return true;
    }

}
