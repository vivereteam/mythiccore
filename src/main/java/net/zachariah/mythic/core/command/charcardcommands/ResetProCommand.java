package net.zachariah.mythic.core.command.charcardcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ResetProCommand implements CommandExecutor {

    private final MythicMain plugin;

    public ResetProCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            if (args.length == 0) {
                sender.sendMessage(ChatColor.RED + "Don't forget to specify a player!");
            }
            OfflinePlayer player = plugin.getServer().getOfflinePlayer(args[0]);
            MythicCharacter character = plugin.getCharacters().get(player);
            character.setProfession(null);
            sender.sendMessage(ChatColor.YELLOW + args[0] + "'s Professions have been reset.");
        } else {
            sender.sendMessage(ChatColor.RED + "Only staff may reset a player's professions!");
        }
        return true;
    }
}
