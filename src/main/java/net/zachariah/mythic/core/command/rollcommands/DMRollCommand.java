package net.zachariah.mythic.core.command.rollcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.roll.Die;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DMRollCommand implements CommandExecutor {

    private final MythicMain plugin;

    public DMRollCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player && sender.hasPermission("roll.info.op")) {
            Player player = (Player) sender;
            if (args.length > 0) {
                boolean rigged = plugin.getRigger().isRigging(player);
                Pattern diePattern = Pattern.compile("[+-]\\d*d\\d+");
                String fullRollString = args[0].startsWith("+") ? args[0] : "+" + args[0];
                Matcher dieMatcher = diePattern.matcher(fullRollString);
                int total = 0;
                StringBuilder parsedRollBuilder = new StringBuilder();
                StringBuilder resultsBuilder = new StringBuilder();
                while (dieMatcher.find()) {
                    String rollString = dieMatcher.group();
                    int multiplier = rollString.startsWith("-") ? -1 : 1;
                    String[] rollSections = rollString.split("d");
                    String diceAmountString = rollSections[0].substring(1);
                    int dieFaces = Integer.parseInt(rollSections[1]);
                    int diceAmount = diceAmountString.isEmpty() ? 1 : Integer.parseInt(diceAmountString);
                    if (diceAmount >= 100){
                        diceAmount = 100;
                    }
                    Die die = new Die(dieFaces);
                    for (int i = 0; i < diceAmount; i++) {
                        int result = die.roll();
                        if (rigged) {
                            result = 1;
                        }
                        total += multiplier * result;
                        if (multiplier == -1) {
                            resultsBuilder.append('-').append(result);
                        } else {
                            if (resultsBuilder.length() > 0) {
                                resultsBuilder.append('+').append(result);
                            } else {
                                resultsBuilder.append(result);
                            }
                        }
                    }
                    String parsedRollString = (multiplier > 0 ? parsedRollBuilder.length() > 0 ? "+" : "" : "-") + diceAmount + "d" + dieFaces;
                    parsedRollBuilder.append(parsedRollString);
                }
                String rollStringWithoutDice = fullRollString.replaceAll("[+-]\\d*d\\d+", "");
                Pattern literalPattern = Pattern.compile("([+-])(\\d+)(?!d)");
                Matcher literalMatcher = literalPattern.matcher(rollStringWithoutDice);
                while (literalMatcher.find()) {
                    String sign = literalMatcher.group(1);
                    int amount = Integer.parseInt(literalMatcher.group(2));
                    if (sign.equals("+")) {
                        total += amount;
                        resultsBuilder.append('+').append(amount);
                    } else if (sign.equals("-")) {
                        total -= amount;
                        resultsBuilder.append('-').append(amount);
                    }
                    parsedRollBuilder.append(sign).append(amount);
                }
                if (parsedRollBuilder.length() > 0) {
                    int radius = 15;
                    String parsedRoll = parsedRollBuilder.charAt(0) == '+' ? parsedRollBuilder.toString().substring(1) : parsedRollBuilder.toString();
                    final int finalTotal = total;
                    sender.sendMessage(new String[] {
                            ChatColor.RED + player.getName() + " rolled " + ChatColor.WHITE + parsedRoll,
                            ChatColor.RED + "(" + ChatColor.GRAY + resultsBuilder.toString() + ChatColor.RED + ")",
                            ChatColor.YELLOW + "Result: " + finalTotal,
                            rigged ? player.getDisplayName() + ": I am a filthy cheater." : ""
                    });
                    plugin.getLogger().info(player.getName() + "'s Result: " + finalTotal + (rigged ? " (Rigged)" : ""));
                    plugin.getRigger().setRigging(player, false);
                } else {
                    sender.sendMessage(ChatColor.RED + "Failed to parse rolls.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Usage: /dmroll [roll]");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to do that!");
        }
        return true;
    }
}
