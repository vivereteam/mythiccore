package net.zachariah.mythic.core.command.proficienciescommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Zac on 2017-11-15.
 */
public class SetToolProfCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetToolProfCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            MythicCharacter character = plugin.getCharacters().get(player);
            character.setToolProf("");
            StringBuilder descriptionBuilder = new StringBuilder(character.getToolProf());
            for (String arg : args) {
                descriptionBuilder.append(arg).append(" ");
            }
            descriptionBuilder.deleteCharAt(descriptionBuilder.length() - 1);
            character.setToolProf(descriptionBuilder.toString());
            sender.sendMessage(ChatColor.GREEN + "Your tool proficiencies has been set");
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
        }

        /*plugin.getConfig().set("cards." + sender.getName() + ".description", "");
        StringBuilder d = new StringBuilder(plugin.getConfig().getString("cards." + sender.getName() + ".description"));
        for (String arg : args) {
            d.append(arg).append(" ");
        }
        plugin.getConfig().set("cards." + sender.getName() + ".description", d.toString());
        sender.sendMessage(ChatColor.GREEN + "Your description has been set.");
        plugin.saveConfig();
        return true;*/
        return true;
    }
}