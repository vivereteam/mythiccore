package net.zachariah.mythic.core.command.utilcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NickCommand implements CommandExecutor {

    private final MythicMain plugin;

    public NickCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0){

            Player player = (Player) sender;

            if (args[0].equalsIgnoreCase("off")){
                player.setDisplayName(player.getName());
                sender.sendMessage(ChatColor.GOLD + "You nick name has been removed.");
            }
            else{
                StringBuilder nameBuilder = new StringBuilder("");
                for (String arg : args) {
                    nameBuilder.append(arg).append(" ");
                }
                nameBuilder.deleteCharAt(nameBuilder.length() - 1);
                if (nameBuilder.length() > plugin.getConfig().getInt("nick_max_length")){
                    sender.sendMessage(ChatColor.RED + "Nick name can't be longer than " + plugin.getConfig().getInt("nick_max_length"));
                    return true;
                }

                MythicCharacter character = plugin.getCharacters().get(player);

                character.setNick(nameBuilder.toString());
                player.setDisplayName(nameBuilder.toString());
                sender.sendMessage(ChatColor.GOLD + "Your nickname has been set.");
            }

        }
        return true;
    }

}
