package net.zachariah.mythic.core.command.charcardcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetGenCommand implements CommandExecutor {

    private final MythicMain plugin;

    public SetGenCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length != 0) {
                Player player = (Player) sender;
                MythicCharacter character = plugin.getCharacters().get(player);
                character.setGender(args[0]);
                sender.sendMessage(ChatColor.GREEN + "Your gender has been set.");
            } else {
                sender.sendMessage(ChatColor.RED + "You must specify a gender.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a a player to perform this command.");
        }
        return true;
    }
}
