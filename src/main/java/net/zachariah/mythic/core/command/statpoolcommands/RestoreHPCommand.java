package net.zachariah.mythic.core.command.statpoolcommands;

import net.zachariah.mythic.core.MythicMain;
import net.zachariah.mythic.core.character.MythicCharacter;
import net.zachariah.mythic.core.stats.MythicStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RestoreHPCommand implements CommandExecutor {

    private final MythicMain plugin;

    public RestoreHPCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to perform this command.");
            return true;
        }
        Player player = (Player) sender;
        MythicCharacter character = plugin.getCharacters().get(player);
        character.setStatValue(MythicStat.CURRENT_HP, character.getStatValue(MythicStat.HP));
        sender.sendMessage(ChatColor.GREEN + "Your HP has been restored!");
        player.getNearbyEntities(15D, 15D, 15D)
                .stream()
                .filter(entity -> entity instanceof Player)
                .map(entity -> (Player) entity)
                .forEach(other -> other.sendMessage(ChatColor.YELLOW + player.getName() + "'s " + ChatColor.GREEN + "HP has been restored."));
        return true;
    }
}
