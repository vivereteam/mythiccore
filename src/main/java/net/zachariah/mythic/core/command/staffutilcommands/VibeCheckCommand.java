package net.zachariah.mythic.core.command.staffutilcommands;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class VibeCheckCommand implements CommandExecutor {

    private final MythicMain plugin;

    public VibeCheckCommand(MythicMain plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("roll.info.op")) {
            if (args.length == 1){
                if (args[0].equalsIgnoreCase("true")){
                    plugin.getConfig().set("VibeCheck", true);
                    sender.sendMessage(ChatColor.GOLD + "You have enabled vibe checking... You monster.");
                    for (Player player : Bukkit.getOnlinePlayers()){
                        if (player.hasPermission("roll.info.op")){
                            player.sendMessage(ChatColor.GOLD + sender.getName() + " has enabled vibe checking... That monster.");
                        }
                    }
                    Bukkit.getScoreboardManager().getMainScoreboard().getTeam("NoCol").setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.ALWAYS);
                }
                else if (args[0].equalsIgnoreCase("false")){
                    plugin.getConfig().set("VibeCheck", false);
                    sender.sendMessage(ChatColor.GOLD + "You have disabled vibe checking.");
                    for (Player player : Bukkit.getOnlinePlayers()){
                        if (player.hasPermission("roll.info.op")){
                            player.sendMessage(ChatColor.GOLD + sender.getName() + " has disabled vibe checking.");
                        }
                    }
                    Bukkit.getScoreboardManager().getMainScoreboard().getTeam("NoCol").setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

                }
                else {
                    sender.sendMessage(ChatColor.RED + "Improper arguments. /vibecheck true/false");
                }
            }
            else{
                sender.sendMessage(ChatColor.RED + "Not enough arguments. /vibecheck true/false");

            }

        }
        else {
            sender.sendMessage(ChatColor.RED + "You don't have permission to use that command!");
        }
        return true;
    }

}
