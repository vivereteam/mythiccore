package net.zachariah.mythic.core.rigger;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MythicRigger {

    private List<UUID> riggingPlayers;

    {
        riggingPlayers = new ArrayList<>();
    }

    public void setRigging(Player player, boolean rigging) {
        if (rigging) {
            riggingPlayers.add(player.getUniqueId());
        } else {
            riggingPlayers.remove(player.getUniqueId());
        }
    }

    public boolean isRigging(Player player) {
        return riggingPlayers.contains(player.getUniqueId());
    }

}
