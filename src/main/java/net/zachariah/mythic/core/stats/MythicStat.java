package net.zachariah.mythic.core.stats;

public enum MythicStat {

    HP("hp"),
    CURRENT_HP("currenthp"),
    TEMP_HP("temphp"),
    EXP("exp"),
    EXP_CAP("exp_cap"),
    STRENGTH("strength"),
    DEXTERITY("dexterity"),
    CONSTITUTION("constitution"),
    INTELLIGENCE("intelligence"),
    WISDOM("resistance"),
    CHARISMA("charisma"),
    STRENGTH_MOD("strengthmod"),
    DEXTERITY_MOD("dexteritymod"),
    CONSTITUTION_MOD("constitutionmod"),
    INTELLIGENCE_MOD("intelligencemod"),
    WISDOM_MOD("resistancemod"),
    CHARISMA_MOD("charismamod"),
    LEVEL("level"),
    CLASS("class");

    private final String configSectionName;

    MythicStat(String configSectionName) {
        this.configSectionName = configSectionName;
    }

    public String getConfigSectionName() {
        return configSectionName;
    }

}
