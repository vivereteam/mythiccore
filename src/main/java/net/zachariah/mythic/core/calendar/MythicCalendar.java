package net.zachariah.mythic.core.calendar;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Calendar;

public class MythicCalendar implements CommandExecutor {
    //  To change epoch:
    //  Take current Epoch in variable
    //  Take difference in Faerunian Days divide by 3 to get IRL days
    //  Take IRL days and convert to epoch (multiply by 86400)
    //  Add to variable to go back in time, subtract to go forward in time
//    private final long epoch = -(14230177198752L);
    private final long epoch = -14171152478752L;
    private final String[] periods = {"Hammer", "Midwinter", "Alturiak", "Ches", "Tarsakh", "Greengrass", "Mirtul",
            "Kythorn", "Flamerule", "Midsummer", "Eleasis", "Eleint", "Harvestide", "Marpenoth", "Uktar",
            "Feast of the Moon", "Nightal"};
    private final int[] durations = {30, 1, 30, 30, 30, 1, 30, 30, 30, 1, 30, 30, 1, 30, 30, 1, 30};

    private long millisSinceEpoch() {
        return millisSinceEpoch(System.currentTimeMillis());
    }

    private long millisSinceEpoch(long realTime) {
        return realTime - epoch;
    }

    public  long millisSinceEpoch(int year, int month, int day, int hour, int minute, int second) {
        long time = dateToMillis(year, month, day, hour, minute, second);
        return time - epoch;
    }

    private long dateToMillis(int year, int month, int day, int hour, int minute, int second) {
        Calendar reference = Calendar.getInstance();
        reference.set(year, month, day, hour, minute, second);
        return reference.getTimeInMillis();
    }

    public  long getVDay() {
        return millisSinceEpoch() / 28800000L;
    }

    private long getVDay(long realMillis) {
        long vMillis = millisSinceEpoch(realMillis);
        return vMillis / 28800000L;
    }

    public  long getVTime() {
        return millisSinceEpoch() % 28800000L;
    }

    private long getVTime(long realMillis) {
        long vMillis = millisSinceEpoch(realMillis);
        return vMillis % 28800000L;
    }

    private String vDayToVDate(long day) {
        long year = day / 365L;
        day %= 365L;
        int index;
        for (index = 0; index < 17; index++) {
            if (day < durations[index]) {
                break;
            }
            day -= durations[index];
        }
        return day + " " + periods[index] + " " + year;
    }

    private String vMillisToVTime(long vMillis) {
        long hour = vMillis / 1200000L;
        long minute = vMillis % 1200000L / 20000L;
        String minuteString = Long.toString(minute);
        return hour + ":" + minuteString;
    }

    private String dateTimeString(long realTime) {
        long vDay = getVDay(realTime);
        long vTime = getVTime(realTime);
        return vMillisToVTime(vTime) + ", " + vDayToVDate(vDay);
    }

    private String dateTimeString() {
        return dateTimeString(System.currentTimeMillis());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if ((cmd.getName().equalsIgnoreCase("calendar")) && ((sender instanceof Player))) {
            sender.sendMessage(ChatColor.GOLD + "It is Currently, " + dateTimeString());
        }
        return true;
    }
}

