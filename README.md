# MythicCore

## Getting started

To get up and running with MythicCore, you're going to need to have Git and JDK 8 (or higher) installed.

First clone the repository with git:

```bash
git clone https://yourname@bitbucket.org/vivereteam/viverecore.git
```

This will clone it to whichever directory you're in.
Once this is done, go inside the viverecore folder, and then build with gradle:

```bash
./gradlew clean build
```

or if on Windows, you can use the batch script:

```batch
gradlew.bat clean build
```

The output will be placed in viverecore/build/libs. You may then copy the JAR onto the server.